import java.util.HashMap;

import com.google.java.contract.Invariant;

@Invariant({
	"true"
})
public class BugzillaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public enum ErrorType {
		UNKNOWN_ERROR,
		DB_LOAD_ERROR,
		DB_SAVE_ERROR,
		ERROR_MISSING_MESSAGE,
		INVALID_STATE_TRANSITION,
		
		USERNAME_NULL,
		USER_ALREADY_REGISTRED,
		INVALID_BUGID,
		USER_ACTION_NOT_PERMITTED,
		PASSWD_NULL,
		PASSWD_INVALID,
		USER_NOT_LOGGED_IN,
		USER_ALREADY_LOGGED_IN,
		USER_NOT_REGISTERED,
		DESCRIPTION_NULL,
		DEVELOPER_ALREADY_ASSIGNED,
		DEVELOPMENT_IN_PROGRESS,
		DEVELOPER_NOT_ASSIGNED,
		DEVELOPER_NOT_ASSIGNED_TO_BUG,
		DEVELOPMENT_NOT_IN_PROGRESS,
		
		NEW_BUG_INVALID_BUGID,
		NEW_BUG_INVALID_DESCRIPTION,
		SET_STATE_TO_NULL,
		SET_STATE_TO_RESOLVED,
		SET_STATE_TO_UNCONFIRMED,
		SET_AS_RESOLVED_NO_SOLUTION_TEXT,
		SET_AS_RESOLVED_UNRESOLVED,
		SET_AS_RESOLVED
	}
	
	private static void addMessage() {

		msgList.put(ErrorType.UNKNOWN_ERROR, "Unknown error");
		msgList.put(ErrorType.DB_LOAD_ERROR, "Error: Failed to load database");
		msgList.put(ErrorType.DB_SAVE_ERROR, "Error: Failed to save database");
		msgList.put(ErrorType.ERROR_MISSING_MESSAGE, "Error: Missing error message in exception");
		msgList.put(ErrorType.INVALID_STATE_TRANSITION, "Error: Bug state cannot be changed from %s to %s");
		
		msgList.put(ErrorType.USERNAME_NULL, "Error: Object username is null");
		msgList.put(ErrorType.USER_ALREADY_REGISTRED, "User exists with this username");
		msgList.put(ErrorType.INVALID_BUGID, "Error: Invalid bug ID");
		msgList.put(ErrorType.USER_ACTION_NOT_PERMITTED, "Error: User does not have permission for this action");
		msgList.put(ErrorType.PASSWD_NULL, "Error: Password is null");
		msgList.put(ErrorType.PASSWD_INVALID, "Error: Password is invalid");
		msgList.put(ErrorType.USER_NOT_LOGGED_IN, "Error: user must be logged in");
		msgList.put(ErrorType.USER_ALREADY_LOGGED_IN, "Error: user is already logged logged in");
		msgList.put(ErrorType.USER_NOT_REGISTERED, "Error: user is not registered");
		msgList.put(ErrorType.DESCRIPTION_NULL, "Error: description can not be empty");
		msgList.put(ErrorType.DEVELOPER_ALREADY_ASSIGNED, "Developer is already assigned");
		msgList.put(ErrorType.DEVELOPMENT_IN_PROGRESS, "Development is already in progress");
		msgList.put(ErrorType.DEVELOPER_NOT_ASSIGNED, "Developer is not assigned");
		msgList.put(ErrorType.DEVELOPER_NOT_ASSIGNED_TO_BUG, "Developer is not assigned to this bug");
		msgList.put(ErrorType.DEVELOPMENT_NOT_IN_PROGRESS, "Development is not in progress");
		
		msgList.put(ErrorType.NEW_BUG_INVALID_BUGID, "Cannot set this as bug ID");
		msgList.put(ErrorType.NEW_BUG_INVALID_DESCRIPTION, "Cannot set this as bug description");
		msgList.put(ErrorType.SET_STATE_TO_NULL, "Cannot set bug state to nothing");
		msgList.put(ErrorType.SET_STATE_TO_RESOLVED, "Cannot set bug state to resolved from here");
		msgList.put(ErrorType.SET_STATE_TO_UNCONFIRMED, "Cannot set bug state to Unconfirmed");
		msgList.put(ErrorType.SET_AS_RESOLVED_NO_SOLUTION_TEXT, "Have to add text for bug solution");
		msgList.put(ErrorType.SET_AS_RESOLVED_UNRESOLVED, "Cannot set resolved bug to unresolved");
		msgList.put(ErrorType.SET_AS_RESOLVED, "Cannot set bug as resolved from here");
		
	}
	
	public static void init() throws BugzillaException {
		
		addMessage();
		
		if(!exInitialized()) {
			throw new BugzillaException(ErrorType.ERROR_MISSING_MESSAGE);
		}
	}
	
	public static boolean exInitialized() {
		return (msgList.size() == ErrorType.values().length && !msgList.containsValue(null));
	}
	
	
	public BugzillaException(ErrorType et) {
		error = et;
		msg = msgList.get(error);
	}
	
	public ErrorType getError() {
		return error;
	}
	
	public String getErrorMsg() {
		return msg;
	}
	
	protected ErrorType error;
	protected String msg;
	protected static HashMap<ErrorType, String> msgList = new HashMap<ErrorType,String>();
}
