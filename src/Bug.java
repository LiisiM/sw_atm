

import java.io.Serializable;

import com.google.java.contract.Ensures;
import com.google.java.contract.Invariant;
import com.google.java.contract.Requires;

/*
 * The class represents a bug in the bugs database.
 * The bug cannot be modified after it is VERIFIED.
 */
@Invariant({
	"ID >= 0",
	"state != null",
	"solutionType != null",
	"solutionInfo != null",
	"bugDescription != null && !bugDescription.isEmpty()",
	"solutionType == Resolution.UNRESOLVED "
	+ "? state != State.RESOLVED && state != State.VERIFIED : true",
	"state == State.CONFIRMED "
	+ "? solutionType == Resolution.UNRESOLVED : true"
})
public class Bug implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	/*
	 * The constructor accepts the Bug ID and description
	 * BugID must not be less than 0. Description must not be empty
	 * The bug is initialized in the state UNCONFIRMED and the resolution 
	 * type is UNRESOLVED
	 */
	@Requires({
	  "id >= 0",
	  "description != null && !description.isEmpty()",
	})
	@Ensures({
	  "ID == old(id)",
	  "bugDescription == old(description)",
	  "state == State.UNCONFIRMED",
	  "solutionType == Resolution.UNRESOLVED",
	})
	public Bug(int id, String description) throws BugzillaException {
		
		if(ID < 0) {
			throw new BugzillaException(BugzillaException.ErrorType.INVALID_BUGID);
		}
		if(id < 0) {
          throw new BugzillaException(BugzillaException.ErrorType.NEW_BUG_INVALID_BUGID);
        }
		if(description == null || description.isEmpty()) {
          throw new BugzillaException(BugzillaException.ErrorType.NEW_BUG_INVALID_DESCRIPTION);
        }
		
		//...
		
		ID = id;
		bugDescription = description;
		state = State.UNCONFIRMED;
		solutionType = Resolution.UNRESOLVED;
		solutionInfo = new String();
	}
	
	@Ensures({
      "result == old(ID)"
    })
	public int getID() {
  	  return ID;
	}

	@Ensures({
      "result == old(bugDescription)"
    })
	public String getBugDescription() {
		return bugDescription;
	}

	@Ensures({
      "result == old(state)"
    })
	public State getState() {
		return state;
	}


	/*
	 * Sets the sate of the bug to any state other than 
	 * RESOLVED and UNCONFIRMED. A bug cannot be set to UNCONFIRMED
	 * because it starts in that state and does not go back to it.
	 * State RESOLVED is set by the method "setAsResolved"
	 */
	@Requires({
	  "st != null",
	  "st == State.CONFIRMED ? state == State.UNCONFIRMED "
	  + "|| state == State.RESOLVED "
	  + "|| state == State.INPROGRESS : true",
	  "st == State.INPROGRESS ? state == State.CONFIRMED : true",
	  "st == State.VERIFIED ? state == State.RESOLVED : true",
	  "st == State.RESOLVED ? state == State.UNCONFIRMED : true",
	  "st != State.RESOLVED",
      "st != State.UNCONFIRMED",
	})
	@Ensures({
      "state == old(st)",
    })
	public void setState(State st) throws BugzillaException {
		
	  //boolean b = (st == State.INPROGRESS) ? (state == State.CONFIRMED) : true;
	  
		if(st == null) {
          throw new BugzillaException(BugzillaException.ErrorType.SET_STATE_TO_NULL);
        }
		if(st == State.CONFIRMED) {
		  if (state != State.UNCONFIRMED && state != State.RESOLVED && state != State.INPROGRESS)
		    throw new BugStateException(state, st);
        }
		if(st == State.INPROGRESS) {
          if (state != State.CONFIRMED)
            throw new BugStateException(state, st);
        }
		if(st == State.VERIFIED) {
          if (state != State.RESOLVED)
            throw new BugStateException(state, st);
        }
		if(st == State.RESOLVED) {
          if (state != State.UNCONFIRMED)
            throw new BugStateException(state, st);
        }
		if(st == State.RESOLVED) {
          throw new BugzillaException(BugzillaException.ErrorType.SET_STATE_TO_RESOLVED);
        }
		if(st == State.UNCONFIRMED) {
          throw new BugzillaException(BugzillaException.ErrorType.SET_STATE_TO_UNCONFIRMED);
        }
		
		this.state = st;
		
		//If state changed from RESOLVED to CONFIRMED then discard the solution type
		if(state == State.CONFIRMED && solutionType != Resolution.UNRESOLVED) {
			solutionType = Resolution.UNRESOLVED;
		}
	}
	
	@Ensures({
      "result == old(solutionType)",
    })
	public Resolution getSolutionType() {
		return solutionType;
	}

	/*
	 * Sets the state of the bug to RESOLVED.
	 * Solution type must not be UNRESOLVED and
	 * solution description must not be empty
	 */
	@Requires({
	  "type != null",
	  "solution != null && !solution.isEmpty()",
	  "type != Resolution.UNRESOLVED",
      "state == State.INPROGRESS || state == State.UNCONFIRMED",
    })
    @Ensures({
      "state == State.RESOLVED",
      "solutionType == old(type)",
      "solutionInfo == old(solution)"
      
    })
	public void setAsResolved(Resolution type, String solution) throws BugzillaException {
  	    if(type == null) {
          throw new BugzillaException(BugzillaException.ErrorType.SET_STATE_TO_NULL);
        }
  	    if(solution == null || solution.isEmpty()) {
          throw new BugzillaException(BugzillaException.ErrorType.SET_AS_RESOLVED_NO_SOLUTION_TEXT);
        }
  	    if(type == Resolution.UNRESOLVED) {
          throw new BugzillaException(BugzillaException.ErrorType.SET_AS_RESOLVED_UNRESOLVED);
        }
  	    if(state != State.INPROGRESS && state != State.UNCONFIRMED) {
  	      throw new BugzillaException(BugzillaException.ErrorType.SET_AS_RESOLVED);
  	    }
	    state = State.RESOLVED;
		solutionType = type;
		solutionInfo = solution;
	}
	
	@Ensures({
      "result == old(solutionInfo)"
    })
	public String getSolutionInfo() {
		return solutionInfo;
	}

	private int ID;
	private String bugDescription;
	private State state;
	private Resolution solutionType;
	private String solutionInfo;
	
	/*
     * If the bug is RESOLVED or VERIFIED, it must have a Resolution type
     * other than UNRESOLVED. The type is set in the "setAsResolved" method.
     * A DEVELOPER may set the value to any other than UNRESOLVED.
     */
    public static enum Resolution {
        UNRESOLVED,
        FIXED,
        DUPLICATE,
        WONTFIX,
        WORKSFORME,
        INVALID
    }
    
    /*
     * The bug must be in one of the following states
     */
    public static enum State {
        UNCONFIRMED,
        CONFIRMED,
        INPROGRESS,
        RESOLVED,
        VERIFIED
    }
}
