import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.java.contract.Invariant;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.google.java.contract.Ensures;
import com.google.java.contract.Requires;

@Invariant({
		"members != null",
		"loggedIn != null",
		"inProgress != null",
		"bugs != null"
})
public class Bugzilla implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public enum MemberType {
		SYSTEMANALYST,
		QUALITYASSURANCE,
		DEVELOPER,
		USER
	}

	@Requires({
			"username != null",
			"passwd != null && ! passwd.isEmpty()",
			"! isRegistered(username)"
	})
	@Ensures({
			"isRegistered(old(username))"
	})
	public void register(String username, String passwd, MemberType type) throws BugzillaException {

		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (passwd == null || passwd.isEmpty()) {
			throwBex(BugzillaException.ErrorType.PASSWD_NULL);
		} else if (isRegistered(username)) {
			throwBex(BugzillaException.ErrorType.USER_ALREADY_REGISTRED);
		}

		members.put(username, getMember(passwd, type));
	}

	@Requires({
			"isRegistered(username)",
			"! isLoggedIn(username)",
			"passwd != null && ! passwd.isEmpty()",
			"passwd == getPasswd(username)"
	})
	@Ensures({
			"isLoggedIn(old(username))"
	})
	public void login(String username, String passwd) throws BugzillaException {
		if (!isRegistered(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_REGISTERED);
		} else if (isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_ALREADY_LOGGED_IN);
		} else if (passwd == null || passwd.isEmpty()) {
			throwBex(BugzillaException.ErrorType.PASSWD_NULL);
		} else if (!passwd.equals(getPasswd(username))) {
			throwBex(BugzillaException.ErrorType.PASSWD_INVALID);
		}
		loggedIn.add(username);
	}

	@Requires({
			"isLoggedIn(username)",
			"isRegistered(username)"
	})
	@Ensures({
			"! isLoggedIn(old(username))"
	})
	public void logout(String username) throws BugzillaException {
		if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (!isRegistered(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_REGISTERED);
		}
		loggedIn.remove(username);
	}

	@Requires({
			"username != null",
			"isLoggedIn(username)",
			"getType(username) == MemberType.USER",
			"description != null"
	})
	@Ensures({
			"bugCount() == old(bugCount()) + 1",
	})
	/*
	 * The method allows a USER to submit a new bug
	 */
	public void submitBug(String username, String description) throws BugzillaException {
		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (getType(username) != MemberType.USER) {
			throwBex(BugzillaException.ErrorType.USER_ACTION_NOT_PERMITTED);
		} else if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (description == null) {
			throwBex(BugzillaException.ErrorType.DESCRIPTION_NULL);
		}

		int bugID = bugs.size();
		bugs.put(bugID, new Bug(bugID, description));
	}

	@Requires({
			"username != null",
			"isLoggedIn(username)",
			"getType(username) == MemberType.SYSTEMANALYST",
			"bugID >= 0",
			"bugExists(bugID)"
	})
	@Ensures({
			"bugCount() == old(bugCount())",
	})
	/*
	 * The method allows a SYSTEMANALYST to confirm a bug
	 */
	public void confirmBug(String username, int bugID) throws BugzillaException {
		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (getType(username) != MemberType.SYSTEMANALYST) {
			throwBex(BugzillaException.ErrorType.USER_ACTION_NOT_PERMITTED);
		} else if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (bugID < 0) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (!bugExists(bugID)) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		}

		getBug(bugID).setState(Bug.State.CONFIRMED);
	}

	@Requires({
			"username != null",
			"isLoggedIn(username)",
			"getType(username) == MemberType.SYSTEMANALYST",
			"bugID >= 0",
			"bugExists(bugID)",
			"solution != null && ! solution.isEmpty()"
	})
	@Ensures({
			"bugCount() == old(bugCount())",
	})
	/*
	 * The method allows a SYSTEMANALYST to invalidate a bug
	 */
	public void invalidateBug(String username, int bugID, String solution) throws BugzillaException {
		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (getType(username) != MemberType.SYSTEMANALYST) {
			throwBex(BugzillaException.ErrorType.USER_ACTION_NOT_PERMITTED);
		} else if (bugID < 0) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (!bugExists(bugID)) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (solution == null || solution.isEmpty()) {
			throwBex(BugzillaException.ErrorType.DESCRIPTION_NULL);
		}

		getBug(bugID).setAsResolved(Bug.Resolution.INVALID, solution);
	}

	@Requires({
			"username != null",
			"isLoggedIn(username)",
			"getType(username) == MemberType.DEVELOPER",
			"bugID >= 0",
			"bugExists(bugID)",
			"! isDeveloperAssigned(username)",
			"! devInProgress(username, bugID)"
	})
	@Ensures({
			"isDeveloperAssigned(old(username))",
			"devInProgress(old(username), old(bugID))",
			"bugCount() == old(bugCount())"
	})
	/*
	 * The method allows a DEVELOPER to start working on the bug
	 */
	public void startDevelopment(String username, int bugID) throws BugzillaException {
		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (getType(username) != MemberType.DEVELOPER) {
			throwBex(BugzillaException.ErrorType.USER_ACTION_NOT_PERMITTED);
		} else if (bugID < 0) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (!bugExists(bugID)) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (isDeveloperAssigned(username)) {
			throwBex(BugzillaException.ErrorType.DEVELOPER_ALREADY_ASSIGNED);
		} else if (devInProgress(username, bugID)) {
			throwBex(BugzillaException.ErrorType.DEVELOPMENT_IN_PROGRESS);
		}

		getBug(bugID).setState(Bug.State.INPROGRESS);
		inProgress.put(username, bugID);
	}

	@Requires({
			"username != null",
			"isLoggedIn(username)",
			"getType(username) == MemberType.DEVELOPER",
			"bugID >= 0",
			"bugExists(bugID)",
			"isDeveloperAssigned(username)",
			"devInProgress(username, bugID)"
	})
	@Ensures({
			"! isDeveloperAssigned(old(username))",
			"! devInProgress(old(username), old(bugID))",
			"bugCount() == old(bugCount())"
	})
	/*
	 * The method allows a DEVELOPER to stop working on the bug
	 */
	public void stopDevelopment(String username, int bugID) throws BugzillaException {
		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (getType(username) != MemberType.DEVELOPER) {
			throwBex(BugzillaException.ErrorType.USER_ACTION_NOT_PERMITTED);
		} else if (bugID < 0) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (!bugExists(bugID)) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (!isDeveloperAssigned(username)) {
			throwBex(BugzillaException.ErrorType.DEVELOPER_NOT_ASSIGNED);
		} else if (!devInProgress(username, bugID)) {
			throwBex(BugzillaException.ErrorType.DEVELOPMENT_NOT_IN_PROGRESS);
		}

		getBug(bugID).setState(Bug.State.CONFIRMED);
		inProgress.remove(username);
	}

	@Requires({
			"username != null",
			"isLoggedIn(username)",
			"getType(username) == MemberType.DEVELOPER",
			"bugID >= 0",
			"bugExists(bugID)",
			"getBug(bugID).getState() == Bug.State.INPROGRESS",
			"solution != null && ! solution.isEmpty()",
			"devInProgress(username, bugID)"
	})
	@Ensures({
			"! isDeveloperAssigned(old(username))",
			"! devInProgress(old(username), old(bugID))",
			"bugCount() == old(bugCount())"
	})
	/*
	 * The method allows DEVELOPER to mark the bug as fixed 
	 */
	public void fixedBug(String username, int bugID, Bug.Resolution resType, String solution) throws BugzillaException {
		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (getType(username) != MemberType.DEVELOPER) {
			throwBex(BugzillaException.ErrorType.USER_ACTION_NOT_PERMITTED);
		} else if (bugID < 0) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (!bugExists(bugID)) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (getBug(bugID).getState() != Bug.State.INPROGRESS) {
			throwBex(BugzillaException.ErrorType.DEVELOPMENT_NOT_IN_PROGRESS);
		} else if (solution == null || solution.isEmpty()) {
			throwBex(BugzillaException.ErrorType.DESCRIPTION_NULL);
		} else if (!devInProgress(username, bugID)) {
			throwBex(BugzillaException.ErrorType.DEVELOPER_NOT_ASSIGNED_TO_BUG);
		}

		getBug(bugID).setAsResolved(resType, solution);
		inProgress.remove(username);
	}

	@Requires({
			"username != null",
			"isLoggedIn(username)",
			"getType(username) == MemberType.QUALITYASSURANCE",
			"bugID >= 0",
			"bugExists(bugID)"
	})
	@Ensures({
			"bugCount() == old(bugCount())"
	})
	/*
	 * The method allows QUALITYASSURANCE to approve the fix (VERIFY) 
	 */
	public void approveFix(String username, int bugID) throws BugzillaException {
		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (getType(username) != MemberType.QUALITYASSURANCE) {
			throwBex(BugzillaException.ErrorType.USER_ACTION_NOT_PERMITTED);
		} else if (bugID < 0) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (!bugExists(bugID)) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		}

		getBug(bugID).setState(Bug.State.VERIFIED);
	}

	@Requires({
			"username != null",
			"isLoggedIn(username)",
			"getType(username) == MemberType.QUALITYASSURANCE",
			"bugID >= 0",
			"bugExists(bugID)"
	})
	@Ensures({
			"bugCount() == old(bugCount())"
	})
	/*
	 * The method allows QUALITYASSURANCE to reject the bug (back to CONFIRMED) 
	 */
	public void rejectFix(String username, int bugID) throws BugzillaException {
		if (username == null) {
			throwBex(BugzillaException.ErrorType.USERNAME_NULL);
		} else if (!isLoggedIn(username)) {
			throwBex(BugzillaException.ErrorType.USER_NOT_LOGGED_IN);
		} else if (getType(username) != MemberType.QUALITYASSURANCE) {
			throwBex(BugzillaException.ErrorType.USER_ACTION_NOT_PERMITTED);
		} else if (bugID < 0) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		} else if (!bugExists(bugID)) {
			throwBex(BugzillaException.ErrorType.INVALID_BUGID);
		}

		getBug(bugID).setState(Bug.State.CONFIRMED);
	}


	/*
	 * Method for throwing exception
	 */
	public static void throwBex(BugzillaException.ErrorType type) throws BugzillaException {
		throw new BugzillaException(type);
	}

	//////////////////////////////////////////////////////////////////////////////////////
	/*
	 * The following private methods can be used for the task
	 */

	private MemberType getType(String username) {
		return members.get(username).getRight();

	}

	private String getPasswd(String username) {
		return members.get(username).getLeft();
	}

	private boolean isRegistered(String username) {
		return members.containsKey(username);

	}

	private boolean isLoggedIn(String username) {
		return loggedIn.contains(username);

	}

	private boolean bugExists(int bugID) {
		return bugs.containsKey(bugID);

	}

	private int bugCount() {
		return bugs.size();
	}

	/*
	 * The method returns the Id of the bug that was created latest
	 */
	private int lastBugID() {
		return (bugs.size() - 1);

	}

	/*
	 * The method returns the Bug object with a given bug ID
	 */
	private Bug getBug(int bugID) {
		return bugs.get(bugID);

	}
	
	/*
	 * The method checks if a developer is already assigned to a bug.
	 * When a developer changes the state of an object to INPROGRESS,
	 * then s/he is consider assigned to the bug.
	 * When the state of the bug is changed by the same developer to 
	 * CONFIRMED (stop working) or RESOLVED (fixed bug) then s/he is not
	 * considered to be assigned.
	 */

	private boolean isDeveloperAssigned(String username) {
		return inProgress.containsKey(username);

	}
	
	/*
	 * The method checks if a developer is is assigned to a specific 
	 * bug ID
	 *
	 * Liisi: I'm sorry, but I needed to change this method
	  *to avoid NullPointerException if username doesn't exist in inProgress
	 */

	private boolean devInProgress(String username, int bugID) {
		return (isDeveloperAssigned(username) && inProgress.get(username) == bugID);
	}

///////////////////////////////////////////////////////////////////////////////////////
	/*
	 * The following methods are not relevant for the assignment task
	 */

	@Ensures({
			"exceptionsInitialized() == true",
			"dataInitialised() == true",
			"fileEnabled == old(saveToFile)",
			"fileEnabled? fileExists() == true: true"
	})
	/*
	 * The constructor initializes the loads and initializes the data
	 * The file operations are enabled only if saveToFile is true.
	 */
	public Bugzilla(boolean saveToFile) throws BugzillaException {

		fileEnabled = saveToFile;
		BugzillaException.init();

		loggedIn = new ArrayList<String>();

		if (!fileEnabled) {
			bugs = new HashMap<Integer, Bug>();
			members = new HashMap<String, Pair<String, MemberType>>();
			inProgress = new HashMap<String, Integer>();
		} else {
			try {
				loadDB();
			} catch (Exception e1) {
				if (fileExists()) {
					File f = new File(filePath);
					if (!f.delete()) {
						throwBex(BugzillaException.ErrorType.DB_LOAD_ERROR);
					}
				}

				bugs = new HashMap<Integer, Bug>();
				members = new HashMap<String, Pair<String, MemberType>>();
				inProgress = new HashMap<String, Integer>();

				try {
					saveDB();
				} catch (Exception e2) {
					if (fileExists()) {
						File f = new File(filePath);
						f.delete();
					}
					throwBex(BugzillaException.ErrorType.DB_LOAD_ERROR);
				}
			}
		}

	}

	@Ensures({
			"isCopyOf(result) == true"
	})
	public Map<Integer, Bug> getBugList() {
		return Collections.unmodifiableMap(bugs);
	}

	private Pair<String, MemberType> getMember(String passwd, MemberType type) {
		return new ImmutablePair<String, MemberType>(passwd, type);
	}

	private boolean isCopyOf(Map<Integer, Bug> map) {
		return map.equals(bugs);
	}

	public void saveData() throws BugzillaException {
		if (fileEnabled) {
			try {
				saveDB();
			} catch (Exception ex) {
				ex.printStackTrace();
				throwBex(BugzillaException.ErrorType.DB_SAVE_ERROR);

			}
		}
	}

	private void saveDB() throws Exception {
		try {
			FileOutputStream fileOut =
					new FileOutputStream(filePath);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			out.writeObject(members);
			out.writeObject(bugs);
			out.writeObject(inProgress);

			out.close();
			fileOut.close();

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;

		}

	}


	@SuppressWarnings("unchecked")
	private void loadDB() throws Exception {

		try {
			FileInputStream fileIn = new FileInputStream(filePath);
			ObjectInputStream in = new ObjectInputStream(fileIn);

			members = (Map<String, Pair<String, MemberType>>) in.readObject();
			bugs = (Map<Integer, Bug>) in.readObject();
			inProgress = (Map<String, Integer>) in.readObject();

			in.close();
			fileIn.close();

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private boolean fileExists() {
		File f = new File(filePath);
		return (f.exists() && !f.isDirectory());
	}

	private boolean dataInitialised() {
		return (members != null &&
				loggedIn != null &&
				bugs != null &&
				inProgress != null);
	}

	private boolean exceptionsInitialized() {
		return BugzillaException.exInitialized();
	}


	private Map<String, Pair<String, MemberType>> members;
	private ArrayList<String> loggedIn;
	private Map<String, Integer> inProgress;
	private Map<Integer, Bug> bugs;

	private boolean fileEnabled;

	private static final String filePath = "bl.bin";
}
