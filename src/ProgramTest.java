
import com.google.java.contract.PreconditionError;
import org.junit.Test;


public class ProgramTest {

	public ProgramTest() {
		try {
			BugzillaException.init();
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	// Bugzilla tests

	//region register

	@Test(expected = PreconditionError.class)
	public void testRegisterNullName() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register(null, "abc", Bugzilla.MemberType.USER);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testRegisterNullPassword() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", null, Bugzilla.MemberType.USER);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testRegistedExistingUser() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);

			// Register same user again
			bz.register("user", "abc", Bugzilla.MemberType.USER);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test
	public void testRegisterUserValid() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	//region login

	@Test(expected = PreconditionError.class)
	public void testLoginNotRegisteredUser() {
		Bugzilla bz = createBugzilla();

		try {
			bz.login("user", "abc");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testLoginAlreadyLoggedInUser() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);
			bz.login("user", "abc");

			//Log in again
			bz.login("user", "abc");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testLoginEmptyPassword() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);

			bz.login("user", "");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testLoginIncorrectPassword() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);

			bz.login("user", "def");
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testLoginValid() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);

			bz.login("user", "abc");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	//region logout

	@Test(expected = PreconditionError.class)
	public void testLogoutNotLoggedInUser() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);

			bz.logout("user");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testLogoutNotRegisteredUser() {
		Bugzilla bz = createBugzilla();

		try {
			bz.logout("user");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test
	public void testLogoutLoggedInUserValid() {
		Bugzilla bz = createBugzilla();

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);
			bz.login("user", "abc");

			bz.logout("user");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	//region submitBug

	@Test(expected = PreconditionError.class)
	public void testSubmitBugNullUsername() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);

		try {
			bz.submitBug(null, "Bug in code");
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PreconditionError.class)
	public void testSubmitBugNotMemberTypeUSER() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);

		try {
			bz.submitBug("developer", "Bug in code");
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PreconditionError.class)
	public void testSubmitBugNotLoggedInUser() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);

		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);

			bz.submitBug("randomuser", "Bug in code");
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PreconditionError.class)
	public void testSubmitBugNullDescription() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);

		try {
			bz.submitBug("user", null);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSubmitBugValid() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);

		try {
			bz.submitBug("user", "Bug in code");
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	//endregion


	//region confirmBug

	@Test(expected = PreconditionError.class)
	public void testConfirmBugNullUsername() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);

		try {
			bz.confirmBug(null, 1);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testConfirmBugNotMemberTypeSYSTEMANALYST() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.confirmBug("user", 1);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testConfirmBugNotLoggedInUser() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.confirmBug("randomuser", 1);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testConfirmBugNegativeBugID() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.confirmBug("sa", -1);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testConfirmBugNoBugExists() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.confirmBug("sa", 25);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test
	public void testConfirmBugValid() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.confirmBug("sa", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	//region invalidateBug

	@Test(expected = PreconditionError.class)
	public void testInvalidateBugNullUsername() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.invalidateBug(null, 0, "Reason");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testInvalidateBugNotMemberTypeSYSTEMANALYST() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.invalidateBug("user", 0, "Reason");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testInvalidateBugNotLoggedInUser() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.invalidateBug("randomuser", 0, "Reason");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testInvalidateBugNegativeBugID() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.invalidateBug("sa", -1, "Reason");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testInvalidateBugNoBugExists() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.invalidateBug("sa", 25, "Reason");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testInvalidateBugNullSolutionString() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.invalidateBug("sa", 0, null);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testInvalidateBugEmptySolutionString() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.invalidateBug("sa", 0, "");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test
	public void testInvalidateBugValid() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		submitBug(bz);

		try {
			bz.invalidateBug("sa", 0, "Reason");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	//region startDevelopment

	@Test(expected = PreconditionError.class)
	public void testStartDevelopmentNullUsername() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		confirmBug(bz);

		try {
			bz.startDevelopment(null, 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStartDevelopmentNotMemberTypeDEVELOPER() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		confirmBug(bz);

		try {
			bz.startDevelopment("user", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStartDevelopmentNotLoggedInUser() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		confirmBug(bz);

		try {
			bz.startDevelopment("randomuser", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStartDevelopmentNegativeBugID() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		confirmBug(bz);

		try {
			bz.startDevelopment("randomuser", -1);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStartDevelopmentNoBugExists() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		confirmBug(bz);

		try {
			bz.startDevelopment("developer", 25);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStartDevelopmentDevAlreadyInProgress() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.startDevelopment("developer", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStartDevelopmentDeveloperAlreadyAssigned() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			// create another bug
			bz.submitBug("user", "Another bug");
			bz.confirmBug("sa", 1);

			// "developer" should not be able to start development, because he already has one active development
			bz.startDevelopment("developer", 1);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test
	public void testStartDevelopmentValid() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		confirmBug(bz);

		try {
			bz.startDevelopment("developer", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	//region stopDevelopment

	@Test(expected = PreconditionError.class)
	public void testStopDevelopmentNullUsername() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.stopDevelopment(null, 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStopDevelopmentNotMemberTypeDEVELOPER() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.stopDevelopment("sa", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStopDevelopmentNotLoggedInUser() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.stopDevelopment("randomuser", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStopDevelopmentNegativeBugID() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.stopDevelopment("developer", -1);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStopDevelopmentNoBugExists() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.stopDevelopment("developer", 25);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PreconditionError.class)
	public void testStopDevelopmentDevNotStarted() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		confirmBug(bz);

		try {
			bz.stopDevelopment("developer", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testStopDevelopmentValid() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.stopDevelopment("developer", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	//endregion


	//region fixedBug

	@Test(expected = PreconditionError.class)
	public void testFixedBugNullUsername() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.fixedBug(null, 0, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testFixedBugNotMemberTypeDEVELOPER() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.fixedBug("user", 0, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testFixedBugNotLoggedInUser() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.fixedBug("randomuser", 0, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testFixedBugNegativeBugID() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.fixedBug("developer", -1, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testFixedBugNoBugExists() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.fixedBug("developer", 25, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testFixedBugNoSolutionDescription() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.fixedBug("developer", 0, Bug.Resolution.FIXED, "");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testFixedBugNotInProgress() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		confirmBug(bz);

		try {
			bz.fixedBug("developer", 0, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testFixedBugAnotherDeveloper() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.register("new_dev", "abc", Bugzilla.MemberType.DEVELOPER);
			bz.login("new_dev", "abc");
			bz.fixedBug("new_dev", 0, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test
	public void testFixedBugValid() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		startDevelopment(bz);

		try {
			bz.fixedBug("developer", 0, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	//region approveFix

	@Test(expected = PreconditionError.class)
	public void testApproveFixNullUsername() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.approveFix(null, 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testApproveFixNotMemberTypeQUALITYASSURANCE() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.approveFix("user", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testApproveFixNotLoggedInUser() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.approveFix("randomuser", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testApproveFixNegativeBugID() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.approveFix("qa", -1);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testApproveFixNoBugExists() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.approveFix("qa", 25);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test
	public void testApproveFixValid() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.approveFix("qa", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	//region rejectFix

	@Test(expected = PreconditionError.class)
	public void testRejectFixNullUsername() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.rejectFix(null, 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testRejectFixNotMemberTypeQUALITYASSURANCE() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.rejectFix("user", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testRejectFixNotLoggedInUser() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.rejectFix("randomuser", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testRejectFixNegativeBugID() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.rejectFix("qa", -1);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test(expected = PreconditionError.class)
	public void testRejectFixNoBugExists() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.rejectFix("qa", 25);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	@Test
	public void testRejectFixValid() {
		Bugzilla bz = createBugzilla();
		createUsers(bz);
		fixedBug(bz);

		try {
			bz.rejectFix("qa", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

	//endregion


	/**
	 * @return new Bugzilla object
	 */
	private Bugzilla createBugzilla() {
		Bugzilla bz = null;

		try {
			bz = new Bugzilla(false);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
		return bz;
	}

	/**
	 * Creates one user of each MemberType and logs them in.
	 * Usernames:
	 * USER - user
	 * DEVELOPER - developer
	 * SYSTEMANALYST - sa
	 * QUALITYASSURANCE - qa
	 *
	 * @param bz Bugzilla object to add users to
	 */
	private void createUsers(Bugzilla bz) {
		try {
			bz.register("user", "abc", Bugzilla.MemberType.USER);
			bz.register("developer", "abc", Bugzilla.MemberType.DEVELOPER);
			bz.register("sa", "abc", Bugzilla.MemberType.SYSTEMANALYST);
			bz.register("qa", "abc", Bugzilla.MemberType.QUALITYASSURANCE);

			bz.login("user", "abc");
			bz.login("developer", "abc");
			bz.login("sa", "abc");
			bz.login("qa", "abc");
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	private void submitBug(Bugzilla bz) {
		try {
			bz.submitBug("user", "Bug in code");
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	private void confirmBug(Bugzilla bz) {
		try {
			submitBug(bz);
			bz.confirmBug("sa", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	private void startDevelopment(Bugzilla bz) {
		try {
			confirmBug(bz);
			bz.startDevelopment("developer", 0);
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}

	private void fixedBug(Bugzilla bz) {
		try {
			startDevelopment(bz);
			bz.fixedBug("developer", 0, Bug.Resolution.FIXED, "Solution");
		} catch (BugzillaException e) {
			e.printStackTrace();
		}
	}


	// Bug tests

	@Test(expected = PreconditionError.class)
	public void testInitBugNegativeId() throws BugzillaException {
		Bug bug = new Bug(-1, "crash on OK press");
	}

	@Test(expected = PreconditionError.class)
	public void testInitBugNullDescription() throws BugzillaException {
		Bug bug = new Bug(1, null);
	}

	@Test(expected = PreconditionError.class)
	public void testInitBugEmptyDescription() throws BugzillaException {
		Bug bug = new Bug(1, "");
	}

	@Test
	public void testInitOk() throws BugzillaException {
		Bug bug = new Bug(1, "asd");
	}

	@Test
	public void testSetStateFromUnconfirmedToConfirmed() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		setBugState(bug, Bug.State.CONFIRMED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromUnconfirmedToInProgress() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		setBugState(bug, Bug.State.INPROGRESS);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromUnconfirmedToResolved() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		setBugState(bug, Bug.State.RESOLVED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromUnconfirmedToVerified() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		setBugState(bug, Bug.State.VERIFIED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromConfirmedToUnconfirmed() throws BugzillaException {
		Bug bug = getConfirmedBug();
		setBugState(bug, Bug.State.VERIFIED);
	}

	@Test
	public void testSetStateFromConfirmedToInProgress() throws BugzillaException {
		Bug bug = getConfirmedBug();
		setBugState(bug, Bug.State.INPROGRESS);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromConfirmedToResolved() throws BugzillaException {
		Bug bug = getConfirmedBug();
		setBugState(bug, Bug.State.RESOLVED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromConfirmedToVerified() throws BugzillaException {
		Bug bug = getConfirmedBug();
		setBugState(bug, Bug.State.VERIFIED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromInProgressToUnconfirmed() throws BugzillaException {
		Bug bug = getInProgressBug();
		setBugState(bug, Bug.State.UNCONFIRMED);
	}

	@Test
	public void testSetStateFromInProgressToConfirmed() throws BugzillaException {
		Bug bug = getInProgressBug();
		setBugState(bug, Bug.State.CONFIRMED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromInProgressToResolved() throws BugzillaException {
		Bug bug = getInProgressBug();
		setBugState(bug, Bug.State.RESOLVED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromInProgressToVerified() throws BugzillaException {
		Bug bug = getInProgressBug();
		setBugState(bug, Bug.State.VERIFIED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromResolvedToUnconfirmed() throws BugzillaException {
		Bug bug = getResolvedBug();
		setBugState(bug, Bug.State.UNCONFIRMED);
	}

	@Test
	public void testSetStateFromResolvedToConfirmed() throws BugzillaException {
		Bug bug = getResolvedBug();
		setBugState(bug, Bug.State.CONFIRMED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromResolvedToInProgress() throws BugzillaException {
		Bug bug = getResolvedBug();
		setBugState(bug, Bug.State.INPROGRESS);
	}

	@Test
	public void testSetStateFromResolvedToVerified() throws BugzillaException {
		Bug bug = getResolvedBug();
		setBugState(bug, Bug.State.VERIFIED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromVerifiedToUnconfirmed() throws BugzillaException {
		Bug bug = getVerifiedBug();
		setBugState(bug, Bug.State.UNCONFIRMED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromVerifiedToConfirmed() throws BugzillaException {
		Bug bug = getVerifiedBug();
		setBugState(bug, Bug.State.CONFIRMED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromVerifiedToInProgress() throws BugzillaException {
		Bug bug = getVerifiedBug();
		setBugState(bug, Bug.State.INPROGRESS);
	}

	@Test(expected = PreconditionError.class)
	public void testSetStateFromVerifiedToResolved() throws BugzillaException {
		Bug bug = getVerifiedBug();
		setBugState(bug, Bug.State.RESOLVED);
	}

	@Test(expected = PreconditionError.class)
	public void testSetAsResolvedWithUnresolvedSolutionType() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		bug.setAsResolved(Bug.Resolution.UNRESOLVED, "asd");
	}

	@Test(expected = PreconditionError.class)
	public void testSetAsResolvedWithEmptyDescription() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		bug.setAsResolved(Bug.Resolution.FIXED, "");
	}

	@Test(expected = PreconditionError.class)
	public void testSetAsResolvedWithNullDescription() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		bug.setAsResolved(Bug.Resolution.FIXED, null);
	}

	@Test
	public void testSetAsResolvedFromUnconfirmedState() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		bug.setAsResolved(Bug.Resolution.FIXED, "asd");
	}

	@Test(expected = PreconditionError.class)
	public void testSetAsResolvedFromConfirmedState() throws BugzillaException {
		Bug bug = getConfirmedBug();
		bug.setAsResolved(Bug.Resolution.FIXED, "asd");
	}

	@Test
	public void testSetAsResolvedFromInProgressState() throws BugzillaException {
		Bug bug = getInProgressBug();
		bug.setAsResolved(Bug.Resolution.FIXED, "asd");
	}

	@Test(expected = PreconditionError.class)
	public void testSetAsResolvedFromResolvedState() throws BugzillaException {
		Bug bug = getResolvedBug();
		bug.setAsResolved(Bug.Resolution.FIXED, "asd");
	}

	@Test(expected = PreconditionError.class)
	public void testSetAsResolvedFromVerifiedState() throws BugzillaException {
		Bug bug = getVerifiedBug();
		bug.setAsResolved(Bug.Resolution.FIXED, "asd");
	}

	private Bug getUnconfirmedBug() {
		Bug bug = null;
		try {
			bug = new Bug(1, "asd");
		} catch (BugzillaException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
		return bug;
	}

	private Bug getConfirmedBug() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		setBugState(bug, Bug.State.CONFIRMED);
		return bug;
	}

	private Bug getInProgressBug() throws BugzillaException {
		Bug bug = getConfirmedBug();
		setBugState(bug, Bug.State.INPROGRESS);
		return bug;
	}

	private Bug getResolvedBug() throws BugzillaException {
		Bug bug = getUnconfirmedBug();
		try {
			bug.setAsResolved(Bug.Resolution.INVALID, "asd");
		} catch (BugStateException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
		return bug;
	}

	private Bug getVerifiedBug() throws BugzillaException {
		Bug bug = getResolvedBug();
		setBugState(bug, Bug.State.VERIFIED);
		return bug;
	}

	private void setBugState(Bug bug, Bug.State state) throws BugzillaException {
		try {
			bug.setState(state);
		} catch (BugStateException e) {
			e.printStackTrace();
			System.out.println(e.getErrorMsg());
		}
	}

}
